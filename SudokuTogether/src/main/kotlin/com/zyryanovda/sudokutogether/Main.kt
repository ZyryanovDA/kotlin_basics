@file:JvmName("Main")
package com.zyryanovda.sudokutogether

import com.sun.tools.javac.Main
import java.io.File
import javax.xml.stream.events.Characters

fun main(){
    val input = readInput()
    println(input)
}

fun readInput(): Map<Pair<Int, Int>, Int> =
    File("input")
        .readLines()
        .withIndex()
        .flatMap { indexedValue ->
            val xCoordinate = indexedValue.index
            indexedValue.value.toCharArray().withIndex()
                .filter { indexedChar -> indexedChar.value != '.'}
                .map { indexedChar ->
                val yCoordinate = indexedChar.index
                    val number = Character.getNumericValue(indexedChar.value)
                (xCoordinate to yCoordinate) to number
            }
        }
        .toMap()


