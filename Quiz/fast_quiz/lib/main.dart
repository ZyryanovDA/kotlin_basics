import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart' show debugPaintSizeEnabled;
import 'package:fast_quiz/home_page.dart';
void main() => runApp(MyApp());



class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fast_Quiz',
      theme: ThemeData(primarySwatch: Colors.indigo,
      textTheme: TextTheme(
        caption: TextStyle(fontSize: 22.0, color: Colors.white),
        ),
        fontFamily: 'Georgia',
      ),

      home: HomePage(),
    );
  }
}


