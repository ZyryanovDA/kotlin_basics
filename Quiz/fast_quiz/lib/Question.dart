import 'package:flutter/material.dart';

class Question {
  final String title;
  final List<Map> answers;

  Question({
    @required this.title,
    @required this.answers,
  });
}


class QuestionData {
  final data = [
    Question(
        title: "Кем хочешь стать?",
        answers: [
          {"answer" : "Ответ1"},
          {"answer" : "Ответ2"},
          {"answer" : "Ответ3", 'isCorrect': 1},
          {"answer" : "Ответ4"}
        ]
    ),
    Question(
        title: "Как хочешь какать?",
        answers: [
          {"answer" : "Ответ1"},
          {"answer" : "Ответ2"},
          {"answer" : "Ответ3", 'isCorrect': 1},
          {"answer" : "Ответ4"}
        ]
    )
  ];
  List<Question> get questions => [...data];
}