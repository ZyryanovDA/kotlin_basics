package com.example.letsimple

import io.reactivex.Observable
import java.lang.RuntimeException
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

fun create(url:String)= Observable.create<String> {
    val urlConnection = URL(url).openCommection() as HttpsURLConnection
    try {
        urlConnection.connect()

        if (urlConnection.responseCode != HttpsURLConnection.HTTP_OK)
            it.onError(RuntimeException(urlConnection.responseMessage))
        else {
            val str = urlConnection.inputStream.bufferedReader().readText()
            it.onNext(str)
        }
    } finally {
        urlConnection.disconnect()
    }
}